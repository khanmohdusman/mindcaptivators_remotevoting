#import libraries
import datetime
import hashlib
import json
import requests
from flask import Flask,jsonify,request
from urllib.parse import urlparse
import copy
import numpy

#part 1 Building a blockchain

class Blockchain:
    def __init__(self, party):
        self.chain = list()
        self.voters = []
        self.user = ""
        self.votes = {}
        self.party = party
        for i in party:
            self.votes[str(i)] = 0
        self.create_block(proof = 1,previous_hash = '0')
        self.nodes = set()

    def create_block(self, proof, previous_hash):
        votes = copy.deepcopy(self.votes)
        voter = copy.deepcopy(self.voters)
        block = {   "index": len(self.chain)+1,
                    "timestamp": str(datetime.datetime.now()),
                    "proof": proof,
                    "voters":self.voters,
                    "votes":self.votes,
                    "previous_hash":previous_hash}
        self.voters = []
        self.chain.append(block)
        return block
    def create_user_block(self, proof, previous_hash):
        block = {   "index": len(self.chain)+1,
                    "timestamp": str(datetime.datetime.now()),
                    "proof": proof,
                    "user": self.user,
                    "previous_hash":previous_hash}
        self.user = ""
        self.chain.append(block)
        return block

    def get_prev_block(self):
        return self.chain[-1]

    def proof_of_work(self,prev_proof):
        check_proof = False
        new_proof = 1
        while check_proof is False:
            hsh = hashlib.sha256(str(new_proof*prev_proof - prev_proof**3).encode()).hexdigest()
            if hsh[:4] == "0000":
                check_proof = True
            else:
                new_proof += 1
        return new_proof

    def hash(self, block):
        en_block = json.dumps(block, sort_keys = True)
        return hashlib.sha256(en_block.encode()).hexdigest()

    def is_valid(self, chain):
        prev_block = chain[0]
        block_index = 1
        while block_index < len(chain):
            block = chain[block_index]
            if block["previous_hash"] != self.hash(prev_block):
                return False
            prev_proof = prev_block["proof"]
            proof = block["proof"]
            hsh = hashlib.sha256(str(proof*prev_proof - prev_proof**3).encode()).hexdigest()
            if hsh[:4] != "0000":
                return False
            prev_block = block
            block_index += 1
        return True
    
    def add_node(self, address):
        parsed_url = urlparse(address)
        self.nodes.add(parsed_url.netloc)

    def add_votes(self,voterids,votes):
        for i in voterids:
            self.voters.append(i)
        parties = self.party
        for i in range(len(votes)):
            self.votes[parties[i]]+=votes[i]
        return previous_block['index'] + 1


    def replace_chain(self):
        network = self.nodes
        longest_chain = None
        max_length = len(self.chain)
        for node in network:
            response = requests.get(f'http://{node}/get_chain')
            if response.status_code == 200:
                length = response.json()['length']
                chain = response.json()['chain']
                if length > max_length and self.is_chain_valid(chain):
                    max_length = length
                    longest_chain = chain
        if longest_chain:
            self.chain = longest_chain
            return True
        return False
    

