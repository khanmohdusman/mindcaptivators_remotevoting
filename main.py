from flask import Flask,render_template,request,json,jsonify,session
import voting_data
from urllib.parse import urlparse
import copy
import datetime
import hashlib
import json
import os
import requests
import http.client
import numpy as np
import pymysql
user = None
app = Flask(__name__)
app.secret_key = os.urandom(24)
db = pymysql.connect("localhost","root","Usman158","mydb")
cursor = db.cursor()
cursor.execute("Select * from party;")
a =cursor.fetchall()
party = []
for i in a:
    party.append(i[1])
blockchain = voting_data.Blockchain(party)
voters = []
i = 0
votes = list(np.zeros(len(party),dtype ='int'))
@app.route("/getvote", methods = ["POST","GET"])
def get_votes():
    global i
    global voters,votes,party
    global user
    voter = user
    voters.append(voter)
    partychose = request.form['party']
    i+=1
    n = party.index(partychose)
    votes[n]+=1
    if i == 5:
        i= 0
        blockchain.add_votes(voters,votes)
        mine_block()
        votes = list(np.zeros(len(party),dtype ='int'))
        voters = []
    else:
        return None
    
@app.route("/mine_block/", methods = ["GET"])
def mine_block():
   prev_block = blockchain.get_prev_block()
   prev_proof = prev_block["proof"]
   proof = blockchain.proof_of_work(prev_proof)
   prev_hash = blockchain.hash(prev_block)
   block = blockchain.create_block(proof,prev_hash)
   response ={'message':'Congratulations, you just mined a block!',
                'index':block['index'],
                'timestamp':block['timestamp'],
               'votes': block['votes'],
               'voters':block['voters'],
                'proof':block['proof'],
                'previous_hash':block['previous_hash']}
   return jsonify(response), 200

@app.route("/mine_user_block/", methods = ["GET"])
def mine_user_block():
   prev_block = blockchain.get_prev_block()
   prev_proof = prev_block["proof"]
   proof = blockchain.proof_of_work(prev_proof)
   prev_hash = blockchain.hash(prev_block)
   block = blockchain.create_user_block(proof,prev_hash)
   response ={'message':'Congratulations, you just mined a block!',
                'index':block['index'],
                'timestamp':block['timestamp'],
               'user':block['user'],
                'proof':block['proof'],
                'previous_hash':block['previous_hash']}
   return jsonify(response), 200

@app.route("/get_chain", methods = ["GET"])
def get_chain():
    response = {"chain": blockchain.chain,
                "length":len(blockchain.chain)}
    return jsonify(response), 200

@app.route('/add_votes', methods = ['POST'])
def add_votes():
    json = request.get_json()
    vote_keys = ['voters', 'votes']
    if not all(key in json for key in vote_keys):
        return 'Some elements of the transaction are missing', 400
    index = blockchain.add_votes(json['voters'], json['votes'])
    response = {'message': f'These votes will be added to Block {index}'}
    return jsonify(response), 201


@app.route("/is_valid", methods = ["GET"])
def is_valid():
    chain = blockchain.chain
    valid = blockchain.is_valid(chain)
    if valid:
        msg = "Good news! Blockchain is valid"
    else:
        msg = "Bad news! Block is not valid"
    response = {"Status": valid,
                "message": msg}
    return jsonify(response), 200

@app.route('/connect_node', methods = ['POST'])
def connect_node():
    json = request.get_json()
    nodes = json.get('nodes')
    if nodes is None:
        return "No node", 400
    for node in nodes:
        blockchain.add_node(node)
    response = {'message': 'All the nodes are now connected. The Hadcoin Blockchain now contains the following nodes:',
                'total_nodes': list(blockchain.nodes)}
    return jsonify(response), 201


@app.route('/replace_chain', methods = ['GET'])
def replace_chain():
    is_chain_replaced = blockchain.replace_chain()
    if is_chain_replaced:
        response = {'message': 'The nodes had different chains so the chain was replaced by the longest one.',
                    'new_chain': blockchain.chain}
    else:
        response = {'message': 'All good. The chain is the largest one.',
                    'actual_chain': blockchain.chain}
    return jsonify(response), 200





@app.route("/home",methods =["GET"])
def home():
    return render_template('index.html')


@app.route("/get-reg")
def register():
    return render_template('reg.html')

@app.route('/save-form',methods=['POST', 'GET'])
def saveform():
   if request.method=='POST':
      name=request.form['username']
      email=request.form['email']
      password = request.form['password']
      voterid = request.form['voterid']
      password = hashlib.sha256(password.encode()).hexdigest()
      voterid = hashlib.sha256(voterid.encode()).hexdigest()
      connection = pymysql.connect(host='localhost',
                             user='root',
                             password='Usman158',
                             db='mydb',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

      with connection.cursor() as cursor:
      # Read a single record
            sql = "INSERT INTO voters (blockchainid,username,password,email,voterid) VALUES (%s,%s,%s,%s,%s)"
            blockchain.user = voterid
            mine_user_block()
            block = blockchain.get_prev_block()
            cursor.execute(sql,(block["index"],name, password, email, voterid))
            connection.commit()
            connection.close()
      return render_template('success.html')
   else:
      return "error"
@app.route('/login')
def login():
    return render_template('login.html')


@app.route('/result',methods=['POST', 'GET'])
def checkuser():
    if request.method=='POST':
     name=request.form['username']
     password = request.form['password']

     password = hashlib.sha256(password.encode()).hexdigest()
     
     try:
  
        # Connect to the database
        connection = pymysql.connect(host='localhost',
                             user='root',
                             password='Usman158',
                             db='mydb',
                             charset='utf8mb4',
                             cursorclass=pymysql.cursors.DictCursor)

        with connection.cursor() as cursor:
      # Read a single record
            sql = f"select id from voters where username='{name}' and password='{password}'"
            check = cursor.execute(sql)

            check1 = cursor.fetchone()
            connection.commit()
            
     finally:
        connection.close()
        if check == 0:
                return render_template('failure.html')
        else:
            global user
            user = name
            return render_template('loginpage.html',message = party)
        
    
      
    else:
      return "error"


@app.route('/logout')
def logout():
   # remove the username from the session if it is there
    global user
    user = None
    


#running the app
app.run(host = "0.0.0.0",port = 80,debug = True)
